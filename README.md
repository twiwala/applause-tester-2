# applause-tester-2

1. To compile BE in folder BE use: `mvn clean install`
2. To run the BE use: `mvn spring-boot:run`
3. To run the tests use: `mvn test`
4. To download FE dependencies use: `npm install` in FE folder.
5. To run FE use: `npm run start` in FE folder.
6. To run karma tests use: `npm run test` in FE folder.
