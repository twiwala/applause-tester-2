package pl.dram.applause2.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import pl.dram.applause2.BaseIntegrationTest;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class TesterControllerTest extends BaseIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Should return [Taybin Rutkin -> 66, Miguel Bautista -> 23] while country is US and device is iPhone 4")
    public void shouldReturnTwoTestersWithDeviceAndCountryFilter() throws Exception {
        mockMvc.perform(
                get("/testers")
                        .param("country", "US")
                        .param("device", "iPhone 4")
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].testerFirstName", is("Taybin")))
                .andExpect(jsonPath("$[0].testerLastName", is("Rutkin")))
                .andExpect(jsonPath("$[0].experience", is(66)))
                .andExpect(jsonPath("$[1].testerFirstName", is("Miguel")))
                .andExpect(jsonPath("$[1].testerLastName", is("Bautista")))
                .andExpect(jsonPath("$[1].experience", is(23)));
    }

    @Test
    @DisplayName("Should return 9 testers while filter is ALL")
    public void shouldReturn9TestersWhenNoFilter() throws Exception {
        mockMvc.perform(
                get("/testers")
                        .param("country", "ALL")
                        .param("device", "ALL")
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(9)));
    }

    @Test
    @DisplayName("Should return 3 testers while only one device = HTC One")
    public void shouldReturnThreeTestersWithDeviceFilter() throws Exception {
        mockMvc.perform(
                get("/testers")
                        .param("country", "ALL")
                        .param("device", "HTC One")
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    @DisplayName("Should return 6 testers while multiple device = HTC One,iPhone 4")
    public void shouldReturnSixTestersWithDeviceFilter() throws Exception {
        mockMvc.perform(
                get("/testers")
                        .param("country", "ALL")
                        .param("device", "HTC One,iPhone 4")
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(6)));
    }
}
