package pl.dram.applause2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Applause2Application {

	public static void main(String[] args) {
		SpringApplication.run(Applause2Application.class, args);
	}

}
