package pl.dram.applause2.model.dto.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class SearchCriteriaVO {
    @Length(min = 1, max = 255)
    @NotNull
    private String country;

    @Length(min = 1, max = 255)
    @NotNull
    private String device;
}
