package pl.dram.applause2.model.view;

import lombok.AllArgsConstructor;

public interface SelectResultView {
    String getTesterFirstName();
    String getTesterLastName();
    Long getExperience();
}
