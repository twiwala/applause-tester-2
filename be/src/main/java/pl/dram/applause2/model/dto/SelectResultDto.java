package pl.dram.applause2.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SelectResultDto {
    private String testerFirstName;
    private String testerLastName;
    private long experience;
}
