package pl.dram.applause2.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@NoArgsConstructor
@Entity
@Data
public class Bug {
    @Id
    private Long id;
    @ManyToOne
    private Tester tester;
    @ManyToOne
    private Device device;
}
