package pl.dram.applause2.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
public class Tester {
    @Id
    private Long id;

    private String firstName;
    private String lastName;

    @Column(nullable = false)
    private String country;
    private LocalDateTime lastLogin;

    @ManyToMany
    private List<Device> testerDevices;
}
