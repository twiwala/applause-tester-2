package pl.dram.applause2.model.entity;

import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@NoArgsConstructor
@Entity
public class Device {
    @Id
    private Long id;
    
    @Column(nullable = false)
    private String description;
}
