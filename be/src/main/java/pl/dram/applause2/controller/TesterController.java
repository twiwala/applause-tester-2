package pl.dram.applause2.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.dram.applause2.model.dto.SelectResultDto;
import pl.dram.applause2.model.dto.vo.SearchCriteriaVO;
import pl.dram.applause2.service.TesterService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("testers")
@AllArgsConstructor
public class TesterController {
    private TesterService testerService;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("")
    public ResponseEntity<List<SelectResultDto>> findExpByDeviceAndCountry(@Valid SearchCriteriaVO searchCriteriaVO) {
        System.out.println(searchCriteriaVO);
        return ResponseEntity.ok(testerService.findExpByDeviceAndCountry(searchCriteriaVO));
    }
}
