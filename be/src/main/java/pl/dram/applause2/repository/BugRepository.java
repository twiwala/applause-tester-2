package pl.dram.applause2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.dram.applause2.model.entity.Bug;
import pl.dram.applause2.model.view.SelectResultView;

import java.util.List;
import java.util.Set;

public interface BugRepository extends JpaRepository<Bug, Long> {
    @Query("SELECT t.firstName AS testerFirstName, t.lastName AS testerLastName, COUNT(b.id) AS experience FROM Tester t INNER JOIN Bug b ON b.tester.id = t.id INNER JOIN Device d ON d.id = b.device.id GROUP BY t.firstName, t.lastName ORDER BY experience DESC")
    List<SelectResultView> findProjections();

    @Query("SELECT t.firstName AS testerFirstName, t.lastName AS testerLastName, COUNT(b.id) AS experience FROM Tester t INNER JOIN Bug b ON b.tester.id = t.id INNER JOIN Device d ON d.id = b.device.id WHERE d.description IN(?1) GROUP BY t.firstName, t.lastName ORDER BY experience DESC")
    List<SelectResultView> findProjectionsByDeviceName(Set<String> devices);


    @Query("SELECT t.firstName AS testerFirstName, t.lastName AS testerLastName, COUNT(b.id) AS experience FROM Tester t INNER JOIN Bug b ON b.tester.id = t.id INNER JOIN Device d ON d.id = b.device.id WHERE t.country IN (?1) GROUP BY t.firstName, t.lastName ORDER BY experience DESC")
    List<SelectResultView> findProjectionsByCountryName(Set<String> testerCountry);

    @Query("SELECT t.firstName AS testerFirstName, t.lastName AS testerLastName, COUNT(b.id) AS experience FROM Tester t INNER JOIN Bug b ON b.tester.id = t.id INNER JOIN Device d ON d.id = b.device.id WHERE d.description IN(?1) AND t.country IN (?2) GROUP BY t.firstName, t.lastName ORDER BY experience DESC")
    List<SelectResultView> findProjectionsByDeviceNameAndTesterCountry(Set<String> deviceName, Set<String> testerCountry);
}
