package pl.dram.applause2.service.selector.filters;

import lombok.Data;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
public class TesterFilter {
    private static String OPTION_ALL = "ALL";
    private static String OPTIONS_SPLITTER = ",";

    private StringFilterOption devices;
    private StringFilterOption countries;

    private TesterFilter(StringFilterOption devices, StringFilterOption countries) {
        this.devices = devices;
        this.countries = countries;
    }

    public static class TesterFilterBuilder {
        private String deviceFilter;
        private String countryFilter;

        public TesterFilterBuilder deviceFilter(String deviceFilter) {
            this.deviceFilter = deviceFilter;
            return this;
        }

        public TesterFilterBuilder countryFilter(String countryFilter) {
            this.countryFilter = countryFilter;
            return this;
        }

        private StringFilterOption parseFilterOption(String value) {
            if (value == null || value.isEmpty() || value.equalsIgnoreCase(OPTION_ALL)) {
                return StringFilterOption.ALL;
            } else {
                return new StringFilterOption(false, Stream.of(value.split(OPTIONS_SPLITTER)).collect(Collectors.toSet()));
            }
        }

        public TesterFilter build() {
            return new TesterFilter(parseFilterOption(deviceFilter), parseFilterOption(countryFilter));
        }
    }
}
