package pl.dram.applause2.service.selector.filters;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@Data
@AllArgsConstructor
public class StringFilterOption {
    public static final StringFilterOption ALL = new StringFilterOption(true, Set.of());

    private final boolean all;
    private final Set<String> values;
}
