package pl.dram.applause2.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.dram.applause2.model.dto.SelectResultDto;
import pl.dram.applause2.model.dto.vo.SearchCriteriaVO;
import pl.dram.applause2.model.view.SelectResultView;
import pl.dram.applause2.repository.BugRepository;
import pl.dram.applause2.service.selector.filters.TesterFilter;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TesterService {

    private BugRepository bugRepository;

    public List<SelectResultDto> findExpByDeviceAndCountry(SearchCriteriaVO searchCriteriaVO) {
        TesterFilter testerFilter = readCriteria(searchCriteriaVO);
        //fast if/else but ugly
        if (testerFilter.getCountries().isAll() && testerFilter.getDevices().isAll()) {
            List<SelectResultView> projections = bugRepository.findProjections();
            return mapProjections(projections);
        } else if (testerFilter.getCountries().isAll()) {
            List<SelectResultView> projections = bugRepository.findProjectionsByDeviceName(testerFilter.getDevices().getValues());
            return mapProjections(projections);
        } else if (testerFilter.getDevices().isAll()) {
            List<SelectResultView> projections = bugRepository.findProjectionsByCountryName(testerFilter.getCountries().getValues());
            return mapProjections(projections);
        } else {
            List<SelectResultView> projections = bugRepository.findProjectionsByDeviceNameAndTesterCountry(testerFilter.getDevices().getValues(), testerFilter.getCountries().getValues());
            return mapProjections(projections);
        }
    }

    private List<SelectResultDto> mapProjections(List<SelectResultView> projection) {
        return projection
                .stream()
                .map(e -> new SelectResultDto(e.getTesterFirstName(), e.getTesterLastName(), e.getExperience()))
                .collect(Collectors.toList());
    }

    private TesterFilter readCriteria(SearchCriteriaVO searchCriteriaVO) {
        return new TesterFilter
                .TesterFilterBuilder()
                .countryFilter(searchCriteriaVO.getCountry())
                .deviceFilter(searchCriteriaVO.getDevice())
                .build();
    }
}
