import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {TesterSearchParamsModel} from '../model/tester-search-params.model';
import {TesterModel} from '../model/tester.model';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TesterService {
  apiURL = 'http://localhost:8080/testers';

  testerData = new BehaviorSubject<TesterModel[]>([]);

  constructor(private http: HttpClient) {
  }

  public loadTesterData(params: TesterSearchParamsModel) {
    this.getTesterByDeviceAndCountry(params).subscribe(v => this.testerData.next(v));
  }

  private getTesterByDeviceAndCountry(params: TesterSearchParamsModel) {
    const httpParams = new HttpParams()
      .append('country', params.country)
      .append('device', params.device);

    return this.http.get<TesterModel[]>(this.apiURL, {params: httpParams});
  }
}
