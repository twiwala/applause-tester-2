export interface TesterModel {
  testerFirstName: string;
  testerLastName: string;
  experience: number;
}
