import {Component, OnInit} from '@angular/core';
import {TesterService} from '../../service/tester.service';

@Component({
  selector: 'app-result-table',
  templateUrl: './result-table.component.html',
  styleUrls: ['./result-table.component.css']
})
export class ResultTableComponent implements OnInit {

  constructor(public testerService: TesterService) {
  }

  ngOnInit() {
    this.testerService.loadTesterData({device: 'ALL', country: 'ALL'});
  }

}
