import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultTableComponent } from './result-table.component';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {TesterModel} from '../../model/tester.model';
import {By} from '@angular/platform-browser';
import {TesterService} from '../../service/tester.service';
import {TesterSearchParamsModel} from '../../model/tester-search-params.model';

describe('ResultTableComponent', () => {
  let component: ResultTableComponent;
  let fixture: ComponentFixture<ResultTableComponent>;

  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let testerService: TesterService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultTableComponent ],
      imports: [HttpClientTestingModule, ReactiveFormsModule, NoopAnimationsModule],
      providers: [TesterService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    httpClient = TestBed.get(HttpClient);
    testerService = TestBed.get(TesterService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain first mocked user', () => {
    const testData: TesterModel[] = [
      {
        testerFirstName: 'dummy name',
        testerLastName: 'dummy last',
        experience: 1
      }
    ];
    testerService.testerData.next(testData);
    fixture.detectChanges();

    const first: HTMLElement = fixture.debugElement.query(By.css('table > tbody > tr:nth-child(1) > td:nth-child(2)')).nativeElement;
    const last: HTMLElement = fixture.debugElement.query(By.css('table > tbody > tr:nth-child(1) > td:nth-child(3)')).nativeElement;
    expect(first.textContent).toEqual('dummy name');
    expect(last.textContent).toEqual('dummy last');
  });
});
