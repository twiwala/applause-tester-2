import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {TesterService} from '../../service/tester.service';
import {Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {TesterSearchParamsModel} from '../../model/tester-search-params.model';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css'],
  animations: [
    trigger('rowAnimation', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-200px)'
        }),
        animate(1000, style({
          opacity: 1,
          transform: 'translateX(0)'
        }))
      ])
    ])
  ]
})
export class SearchFormComponent implements OnInit, OnDestroy {

  searchData = new FormGroup({
    country: new FormControl('ALL'),
    device: new FormControl('ALL')
  });

  subscription: Subscription;

  constructor(public testerService: TesterService) {
  }

  ngOnInit() {
    this.subscription = this.searchData
      .valueChanges
      .pipe(
        debounceTime(500),
        distinctUntilChanged((x, y) => JSON.stringify(x) === JSON.stringify(y))
      ).subscribe((val) => {
        this.testerService.loadTesterData(val as TesterSearchParamsModel);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
